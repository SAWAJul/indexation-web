import json
import string
import nltk
import spacy

from nltk.stem import WordNetLemmatizer
from nltk.stem.snowball import FrenchStemmer
from typing import List
from collections import Counter


def tokens_preprocessing(text: str) -> List:
    """
        Définition du preprocessing à faire sur les tokens
        Il s'agit ici de faire les opérations suivantes :
            - suppression des ponctuations
            - downcasting
            - lemmatisation
        
        Entrée:
            tokens: Liste contenant les tokens
        
        Sortie:
            Liste contenant les tokens preproced
    """
    # Downcasting et Tokenization avec du split sur l'espace
    tokens = text.lower().split()

    # Suppresion des ponctuations comme token
    tokens = [token for token in tokens if token not in string.punctuation]

    # Téléchargement de Wordnet pour la lemmatization
    if not nltk.data.find('corpora/wordnet.zip'):
        nltk.download('wordnet', download_dir=".", quiet=True)

    # Initialiser le lemmatizer de WordNet
    lemmatizer = WordNetLemmatizer()

    # Obtenir les lemmes de chaque mot dans la liste
    lemmas = [lemmatizer.lemmatize(token) for token in tokens]

    stemmer = FrenchStemmer()

    stems = [stemmer.stem(lem) for lem in lemmas]

    return stems


def get_title_index(crawler_file: str) -> None:
    """
        Fonction permettant de créer l'index du titre d'un ensemble de documents

        Entrée:
            file : Fichier json contenant les documents

        Sortie:

    """
    # Charger le modèle spaCy pour le français
    process = spacy.load('fr_core_news_sm')

    # Lecture de la base de documents
    with open(crawler_file, 'r') as json_file:
        urls = json.load(json_file)

    # Liste d'index vide
    index_non_stem = dict()
    index_posi = dict()

    # Variable qui compte le nombre total de tokens
    tokens_total = 0

    # Dictionnaire des métadonnées
    metadata = {
        "Nombre total de documents": len(urls)
    }

    # Dictionnaire permettant pour chaque document de stocker son nombre de tokens
    token_par_champ = dict()

    with process.select_pipes(enable=["tok2vec", "lemmatizer"]):
        # Parcours de tous les urls contenu dans le fichier
        for ind, url in enumerate(urls):

            text = url['title']
            
            # Tokenization du texte
            text_tokenized = [token.lemma_.lower() for token in process(text=text)]

            # Récupération du nombre de tokens
            token_tot_champ = len(text_tokenized)
            token_par_champ[url['url']] = token_tot_champ

            # Création de la liste inversée de tokens
            tokens_count = Counter(text_tokenized)
            for token in tokens_count:
                if token in index_non_stem:
                    index_non_stem[token].append(ind)
                    index_posi[token][ind] = {
                        "position": [index for index, value in enumerate(text_tokenized) if value == token],
                        "count": tokens_count[token]
                    }
                else:
                    index_non_stem[token] = [ind]
                    index_posi[token] = {
                        ind: {
                            "position": [index for index, value in enumerate(text_tokenized) if value == token],
                            "count": tokens_count[token]
                        }
                    }

            tokens_total += token_tot_champ

    # Le cas où l'on ajoute le stemming des différents tokens
    index_stem = dict()

    for ind, url in enumerate(urls):
        text = url['title']

        # Preprocessing des différents tokens pour le cas du stemming
        tokens_preproced = tokens_preprocessing(text)

        # Création de la liste inversée de tokens
        tokens_count = Counter(tokens_preproced)
        for token in tokens_count:
            if token in index_stem:
                index_stem[token].append(ind)
            else:
                index_stem[token] = [ind]


    # Ajout des statistiques à la base des métadonnées
    metadata["Nombre de tokens global"] = tokens_total
    metadata["Moyenne des tokens par documents"] = tokens_total/len(urls)
    metadata["Nombre de tokens par champs"] = token_par_champ

    # Sauvegarde des index dans un fichier json
    with open("title.non_pos_index.json", 'w') as json_file:
        json.dump(index_non_stem, json_file, ensure_ascii=False)

    with open("mon_stemmer.title.non_pos_index.json", 'w') as json_file:
        json.dump(index_stem, json_file, ensure_ascii=False)

    with open("title.pos_index.json", 'w') as json_file:
        json.dump(index_posi, json_file, ensure_ascii=False)

    # Sauvegarde des métadonnées
    with open("metadata.json", 'w') as json_file:
        json.dump([metadata], json_file, ensure_ascii=False)


def get_content_index(crawler_file: str) -> None:
    """
        Fonction permettant de créer l'index du contenu d'un ensemble de documents

        Entrée:
            file : Fichier json contenant les documents

        Sortie:
    """
    # Charger le modèle spaCy pour le français
    process = spacy.load('fr_core_news_sm')

    # Lecture de la base de documents
    with open(crawler_file, 'r') as json_file:
        urls = json.load(json_file)

    # Dictionnaire permettant pour chaque document de stocker son nombre de tokens
    index_content_posi = dict()

    with process.select_pipes(enable=["tok2vec", "lemmatizer"]):
        # Parcours de tous les urls contenu dans le fichier
        for ind, url in enumerate(urls):

            text = url['content']
            
            # Tokenization du texte
            text_tokenized = [token.lemma_.lower() for token in process(text=text)]

            # Création de la liste inversée de tokens
            tokens_count = Counter(text_tokenized)
            for token in tokens_count:
                if token in index_content_posi:
                    index_content_posi[token][ind] = {
                        "position": [index for index, value in enumerate(text_tokenized) if value == token],
                        "count": tokens_count[token]
                    }
                else:
                    index_content_posi[token] = {
                        ind: {
                            "position": [index for index, value in enumerate(text_tokenized) if value == token],
                            "count": tokens_count[token]
                        }
                    }

    # Le cas où l'on ajoute le stemming des différents tokens
    index_content_stem = dict()

    for ind, url in enumerate(urls):
        text = url['content']

        # Preprocessing des différents tokens pour le cas du stemming
        tokens_preproced = tokens_preprocessing(text)

        # Création de la liste inversée de tokens
        tokens_count = Counter(tokens_preproced)
        for token in tokens_count:
            if token in index_content_stem:
                index_content_stem[token].append(ind)
            else:
                index_content_stem[token] = [ind]

    # Sauvegarde des index dans un fichier json
    with open("mon_stemmer.content.non_pos_index.json", 'w') as json_file:
        json.dump(index_content_stem, json_file, ensure_ascii=False)

    with open("content.pos_index.json", 'w') as json_file:
        json.dump(index_content_posi, json_file, ensure_ascii=False)