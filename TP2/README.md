## Construction d'un index minimal

Ce TP permet de créer un index minimal qui part d'une liste de site déjà crawler et construit un index pour le titre et le contenu des différents urls.

## Fonctionnement du code

Le code fonctionne de manière suivante :
- Vérification du paramètre d'entrée, c'est-à-dire la spécification d'un fichier contenant les urls crawler avec leurs informations
- Lis le contenu du fichier crawler et enregistre les différents urls
- Pour la construction des index:
    - Tout d'abord, le texte du titre subi un preprocessing selon le fait qu'on veut utiliser un stemmer ou non :
        - Lorsque l'on n'utilise pas de stemmer, le preprocessing consiste à utiliser le traitement du package `spaCy` pour tokenizer le texte et récupérer les lemmes des différents tokens
        - Lorsque l'on utilise un stemmer, le preprocessing consiste à appliquer un split du texte du titre avec l'espace, utiliser le lemmatizer de Wordnet et l'un des Stemmer de Wordnet qui est `FrenchStemmer`.
    - Avec la liste des tokens lemmatizer ou stemmer, un `Counter` est appliquer pour obtenir pour chaque token le nombre de fois que celui ci apparaît dans la liste et on recupère aussi les index de ce token dans la liste des tokens
    - Pour chaque token, construire le dictionnaire d'index en ajoutant l'identifiant du document dans lequel se trouve le token dans la liste déjà présente lorsque le token avait déja été trouvé dans un autre document, ou ajouter une nouvelle clé au dictionnaire avec pour valeur une liste contenant l'identifiant du document dans lequel se trouve le token
- Cette construction des index est valide pour les index du titre et du contenu. Néanmoins, pour le contenu des documents, seuls l'index positionnel et l'index avec stemmer ont été construits.
- Au final, tous les dictionnaires d'index calculés sont enregistrés dans des fichiers json.
- Les métadonnées sont aussi calculées et aussi enregistrées dans un fichier json.

## Démarrage rapide

cloner le dépôt git :

```
git clone https://gitlab.com/SAWAJul/indexation-web.git
```

Se placer dans le dossier de l'application, via les commandes:

```
cd indexation-web/TP2
```

Enfin lancer la construction d'index avec :

```
python3 main.py crawled_urls.json
```

## Auteur

SAWADOGO Julien