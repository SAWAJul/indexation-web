import argparse
from Index.index import get_title_index, get_content_index

def main(crawler_file):
    get_title_index(crawler_file)
    get_content_index(crawler_file)
    print("Done")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Take a crawler file and construct it index file")
    parser.add_argument("filename", help="Path to the crawler json file")

    args = parser.parse_args()

    main(args.filename)