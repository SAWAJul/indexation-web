import argparse
from Index.index import get_docs_for_request

def main(request, max_docs, filtre_OU):
    get_docs_for_request(request, max_docs, bool(filtre_OU))
    print('Done !!')

def validate_filtre_OU(value):
    if value not in ['0', '1']:
        print("entrer")
        raise argparse.ArgumentTypeError("Le paramètre filtre_OU doit être soit 0 ou 1")
    return value

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Take a user request and return docs that contain it")
    parser.add_argument("requete", help="Requête de l'utilisateur")
    parser.add_argument("--max_docs", type=int, default=10, help="Nombre maximal de document à retourner")
    parser.add_argument("--filtre_OU", type=validate_filtre_OU, default=0, help="Filtre ET ou filtre OU")

    args = parser.parse_args()

    main(args.requete, args.max_docs, int(args.filtre_OU))