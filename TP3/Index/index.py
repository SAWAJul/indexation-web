import json
import nltk

from nltk.corpus import stopwords
from typing import List
from rank_bm25 import BM25Okapi
from statistics import mean

def request_preprocessing(request: str) -> List:
    """
        Fonction permettant d'appliquer le preprocessing lors de la
        construction des index de pages à la requête de l'utilisateur

        Example:
            - split avec l'espace
            - lowerisation

        Entrée:
            request : requête de l'utilisateur

        Sortie:
            requête de l'utilisateur preprocessé
    """
    return request.lower().split()


def docs_retrieval(request_tokens: List, filtre_OU=False) -> List:
    """
        Fonction permettant de récupérer tous les documents qui
        matchent en titre ou contenu avec la requête de l'utilisateur

        Entrée:
            request_tokens : ensemble de tokens de la reuquête de l'utilisateur

        Sortie:
            Liste des documents qui matchent avec la requête de l'utilisateur
            (Pour un début, le match se fera par un AND)
    """
    # Lecture du fichier des index du titre
    with open("title_pos_index.json", "r") as json_file:
        title_index = json.load(json_file)

    res1 = set()

    # Parcours des tokens de la requête de l'utilisateur
    for token in request_tokens:
        # Vérification de la présence du token
        if token not in title_index:
            # Lorsque le token n'est pas présent dans la liste des index de
            # title, on arrête la boucle et on passe aux index du contenu
            if not filtre_OU:
                res1 = set()
                break
        else:
            # Lorsque le token est présent, on récupère la liste des documents
            # où il se retrouve en titre
            if filtre_OU:
                res1 = res1.union(title_index[token].keys())
            else:
                if len(res1) != 0:
                    res1 = res1 & set(title_index[token].keys())
                    # Lorsque l'intersection est vide alors on arrête le process
                    if len(res1) == 0:
                        break
                else:
                    res1 = set(title_index[token].keys())
    
    # Même traitement que précedemment avec cette fois les indexs de contenu des docs
    with open("content_pos_index.json", "r") as json_file:
        content_index = json.load(json_file)
    
    res = set()
    
    for token in request_tokens:

        if token not in content_index:
            if not filtre_OU:
                break
        else:
            if filtre_OU:
                res = res.union(content_index[token].keys())
            else:
                if len(res) != 0:
                    res = res & set(content_index[token].keys())
                    if len(res) == 0:
                        break
                else:
                    res = set(content_index[token].keys())
    
    res = list(res.union(res1))
    return res


def ranking_function(docs: List, request_tokens: List) -> List:

    # Télécharger les ressources nécessaires pour nltk (si ce n'est pas déjà fait)
    if not nltk.data.find('corpora/stopwords.zip'):
        nltk.download('stopwords', download_dir=".", quiet=True)

    # Obtenir la liste des stop words pour une langue spécifique (par exemple, l'anglais)
    stop_words = set(stopwords.words('english'))

    # Lecture du fichier des index du titre
    with open("title_pos_index.json", "r") as json_file:
        title_index = json.load(json_file)

    scores = dict()
    for id in docs:
        score = 0
        for token in request_tokens:
            if token in title_index:
                res = title_index[token]
                if id in res:
                    position_moyenne = mean(res[id]['positions'])
                    count = res[id]['count']
                    if token in stop_words:
                        score += 0.5*(1/(position_moyenne+1) + count)
                    else:
                        score += 1/(position_moyenne+1) + count
        scores[id] = {'title': score}

    # Lecture du fichier des index du titre
    with open("content_pos_index.json", "r") as json_file:
        content_index = json.load(json_file)

    for id in docs:
        score = 0
        for token in request_tokens:
            if token in content_index:
                res = content_index[token]
                if id in res:
                    position_moyenne = mean(res[id]['positions'])
                    count = res[id]['count']
                    if token in stop_words:
                        score += 0.5*(1/(position_moyenne+1) + count)
                    else:
                        score += 1/(position_moyenne+1) + count
        scores[id]['content'] = score
    
    return scores


def score_bm25(docs: List, request_tokens: List) -> dict:
    """
        Calcul du score de bm25

        Entrée :
            docs : liste des documents
            request_tokens : requête de l'utilisateur splitter en token

        Sortie :
            dictionnaire des documents avec chacun son score bm25
    """
    result = dict()

    with open("documents.json", "r") as json_file:
        documents = json.load(json_file)
    
    title_tokens = []
    content_tokens = []
    for id in docs:
        document = documents[int(id)]
        title_tokens.append(request_preprocessing(document['title']))
        content_tokens.append(request_preprocessing(document['content']))

    # Création de l'objet BM25
    bm25_title = BM25Okapi(title_tokens)
    bm25_content = BM25Okapi(content_tokens)

    # Calcul des scores BM25 pour chaque document par rapport à la requête
    scores_title = bm25_title.get_scores(request_tokens)
    scores_content = bm25_content.get_scores(request_tokens)

    for i, id in enumerate(docs):
        result[id] = {'title_bm25': scores_title[i], 'content_bm25': scores_content[i]}

    return result


def get_docs_after_ranking(docs: List, request_tokens: List, max_docs: int) -> List:
    """
        Fonction permettant de ranker les différents documents et de
        retourner les max_docs premiers

        Entrée :
            docs : liste des documents à ranker
            max_docs : nombre maximal de documents à retourner

        Sortie:
            Liste des max_docs premiers documents après le ranking
    """
    bm25 = score_bm25(docs=docs, request_tokens=request_tokens)
    ranking = ranking_function(docs=docs, request_tokens=request_tokens)
    scores = [0.28*ranking[id]['title'] + 0.12*ranking[id]['content'] + 0.42*bm25[id]['title_bm25'] + 0.18*bm25[id]['content_bm25'] for id in docs]
    sorted_indices = [i for i, _ in sorted(enumerate(scores), key=lambda x: x[1], reverse=True)][:max_docs]

    result = [docs[i] for i in sorted_indices]
    return result


def get_docs_infos(docs: List, nbdocs: int, filtre_OU: bool) -> None:
    """
        Fonction permettant de récupérer l'url et le titre des articles
        sélectionnés après l'étape de ranking et les enregistrer dans le
        fichier results.json

        Entrée:
            docs : liste des 10 meilleurs documents
            nbdocs : nombre de documents ayant survécu au premier filtrage
            filtre_OU : specifiction d'un filtre avec ou sans OU sur les tokens de la requete 

        Sortie:
            None 
    """
    # Ouverture du fichier contenant les informations sur
    # les documents (url, id, titre, content)
    with open("documents.json", "r") as json_file:
        documents = json.load(json_file)
    
    # Sélection des documents concernés
    docs_info = [documents[int(id)] for id in docs]
    # Récupération du titre et de l'url des documents concernés
    res = [{'title': doc['title'], 'url': doc['url']} for doc in docs_info]
    arbre = "OR" if filtre_OU else "AND"
    results = [f"{nbdocs} documents contiennent la requête de l'utilisateur avec un arbre de {arbre}",
               {f"Les {len(docs)} meilleurs sont :": res}]

    # Enregistrement des résultats
    with open('results.json', 'w') as json_file:
        json.dump(results, json_file, ensure_ascii=False, indent=3)


def get_docs_for_request(request: str, max_docs: int, filtre_OU: bool) -> None:
    """
        Fonction permettant de récupérer les différents documents pour la requête
    """
    request_tokens = request_preprocessing(request=request)
    docs = docs_retrieval(request_tokens, filtre_OU)
    nbdocs = len(docs)
    if max_docs > nbdocs:
        max_docs = nbdocs
        print("Warning !!!!\nLe nombre maximal de documents démandés dépasse le nombre de documents qui match la requête\nTous les documents seront donc renvoyés")
    docs = get_docs_after_ranking(docs=docs, request_tokens=request_tokens, max_docs=max_docs)
    get_docs_infos(docs=docs, nbdocs=nbdocs, filtre_OU=filtre_OU)