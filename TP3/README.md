## Expansion de requête et ranking

Ce TP permet de ressortir pour une requête donnée la liste des meilleurs documents faisant référence à la requête.

## Fonctionnement du code

Le code fonctionne de manière suivante :
- Vérification des paramètres d'entrée, c'est-à-dire la spécification de la requête, du nombre maximal de documents à retourner et de l'utilisation d'un filtre OU ou non pour le matching de la requête et des documents
- Une première fonction permettant d'appliquer le preprocessing utilisé pour construire les index à la requête de l'utilisateur. Dans ce cas, le preprocessing consiste en un split sur l'espace et le casting des différents tokens en miniscules
- La deuxième fonction permet de sélectionner les différents documents qui contiennent les tokens de la requête avec le filtre spécifié. Cette fonction récupère la liste des documents contenant la requête dans son titre ou dans son contenu. Les objets `set` sont utilisés pour permettre de sélectionner une fois un document qui apparaît plusieurs fois.
- La fonction suivante permet de calculer un score de ranking pour tous les documents qui contiennent la requête. Cette fonction de score se calcule de la manière suivante :
    - Pour chaque document, on récupère la position moyenne et le nombre de fois que chaque token de la requête apparaît dans le document, soit dans le titre, soit dans le contenu ;
    - Avec ces deux informations et pour chaque token, on calcul le score commme la somme de l'inverse de la position moyenne et du nombre de fois que le token apparaît dans le document (en titre ou contenu). L'inverse de la position moyenne parce qu'on fait l'hypothèse que lorsqu'un token apparaît en début d'un contenu ou titre alors ce document est plus important.
Lorsque le token est un `stop_word` ce score est divisé par deux.
- La prochaine fonction permet de calculer le score `bm25` de chaque document
- Avec ces deux scores, on calcule le score final pour chaque document et on récupère les `max_docs` ayant les meilleurs scores. Le score `bm25` correspond à 60% du score final et le score de ranking à 40%. Dans chaque cas, le score du titre correspond à 70% et celui du contenu à 30%.

## Démarrage rapide

cloner le dépôt git :

```
git clone https://gitlab.com/SAWAJul/indexation-web.git
```

Se placer dans le dossier de l'application, via la commande:

```
cd indexation-web/TP3
```

Enfin lancer la construction d'index avec :

```
python3 main.py "Un mauvais souvenir" --max_docs 10 --filtre_OU 1
```

## Auteur

SAWADOGO Julien
