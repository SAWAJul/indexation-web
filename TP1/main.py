import argparse
import time
from crawler.crawler import crawler

def main(filename, politeness, max_urls):
    crawler(filename=filename, politeness=politeness, max_urls=max_urls)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Parse a text file and extract each line as a page URL.")
    parser.add_argument("filename", help="Path to the text file")
    parser.add_argument("--politeness_delay", type=int, default=3, help="The politeness between two pages")
    parser.add_argument("--max_urls", type=int, default=50, help="Maximum number of pages to crawl")

    args = parser.parse_args()

    main(args.filename, args.politeness_delay, args.max_urls)