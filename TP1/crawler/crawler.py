import random
import time
import concurrent.futures
from urllib import request, parse, robotparser
from bs4 import BeautifulSoup
from urllib.error import HTTPError

random.seed(1234)


def is_valid_url(url: str):
    try:
        result = parse.urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


def is_crawl_allowed(base_url):
    cleaned_url = base_url.strip()
    parsed_url = parse.urlparse(cleaned_url)
    # Créer un objet RobotFileParser
    rp = robotparser.RobotFileParser()
    rp.set_url(parse.urljoin(parsed_url.scheme + "://" + parsed_url.netloc, "/robots.txt"))
    # Vérifier si l'utilisateur est autorisé à crawler l'URL spécifiée
    rp.read()
    return rp.can_fetch("*", base_url)


def has_path(url):
    parsed_url = parse.urlparse(url)
    return bool(parsed_url.path != "/")


def get_links_from_page(url):
    try:
        # Effectuer la requête GET
        with request.urlopen(url, timeout=2) as response:
            html = response.read()

        # Analyser le HTML de la page
        soup = BeautifulSoup(html, 'html.parser')

        # Récupérer tous les liens de la page
        links = soup.find_all('a', href=True)

        # Construire les URL absolues
        absolute_links = [parse.urljoin(url, link['href']) for link in links]
        return random.sample(absolute_links, k=5) if len(absolute_links) > 5 else absolute_links

    except Exception as e:
        return []
    

def get_sitemap_urls(site_url):
    try:
        # Fetch the robots.txt file to check for a sitemap entry
        robots_url = parse.urljoin(site_url, "/robots.txt")
        with request.urlopen(robots_url) as response:
            robots_content = response.read().decode("utf-8")

        # Search for the sitemap entry in robots.txt
        sitemap_entry = []
        for line in robots_content.split('\n'):
            if line.lower().startswith("sitemap:"):
                sitemap_entry.append(line.lower().split("sitemap:")[1].strip())

        urls = []
        # If a sitemap entry is found, fetch and parse the sitemap
        if len(sitemap_entry) != 0:
            for sitemap_url in sitemap_entry:
                with request.urlopen(sitemap_url) as sitemap_response:
                    sitemap_content = sitemap_response.read()

                # Parse the sitemap XML with Beautiful Soup
                soup = BeautifulSoup(sitemap_content, 'xml')

                # Extract URLs from <loc> elements
                urls.extend([loc.text for loc in soup.find_all('loc')])

            return random.sample(urls, k=5) if len(urls) > 5 else urls
        else:
            return []

    except HTTPError as e:
        return []
    except Exception as e:
        return []
    

def crawler_url(url : str):
    if not is_valid_url(url=url):
        return []
    print(f"Crawling {url}")
    url = url.strip()
    
    if is_crawl_allowed(base_url=url):
        if not has_path(url):
            finded_urls = get_sitemap_urls(url)
        else:
            finded_urls = get_links_from_page(url=url)
    else:
        print(f'{url} is not crawlable')
        finded_urls = []
    
    return finded_urls


def crawler(filename: str, politeness: int, max_urls: int) -> None:
    try:
        with open(filename, 'r') as file:
            seed_urls = file.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
    
    frontier = seed_urls
    crawled_webpages = list()

    # Nombre de threads à utiliser
    nombre_threads = 5

    while len(crawled_webpages) < max_urls:
        urls_for_crawling = [url for url in frontier if url not in crawled_webpages]

        if len(urls_for_crawling) == 0:
            print("Plus de liens à explorer")
            break

        res = list()
        with concurrent.futures.ThreadPoolExecutor(max_workers=nombre_threads) as executor:
            resultats = list(executor.map(crawler_url, urls_for_crawling))

            for param, result in zip(urls_for_crawling, resultats):
                if len(result) != 0:
                    res.extend(result)
                    crawled_webpages.append(param)
                time.sleep(politeness)

        for url_found in res:
            if (url_found not in frontier) and (url_found not in crawled_webpages):
                frontier.append(url_found)
        
        [frontier.remove(url) for url in urls_for_crawling]

        print(f"Visited URLs : {len(crawled_webpages)}")
        print(f"URLs to visit : {len(frontier)}\n")


        if len(frontier) == 0:
            print("Plus de liens à explorer")
            break

    
    with open('crawled_webpages.txt', 'w') as text_file:
        for url_crawled in crawled_webpages[:max_urls]:
            text_file.write(url_crawled + '\n')