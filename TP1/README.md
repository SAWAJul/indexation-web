# indexation-web

## Construction d'un crawler

Ce TP permet de créer un crawler minimal qui part d'un seed de site et retourne tous les sites qu'il a réussi à trouver en partant de cette seed.

## Fonctionnement du code

Le code fonctionne de manière suivante :
- Vérification des paramètres d'entrée, c'est-à-dire la spécification d'un fichier seed, du temps de politeness et du nombre maximal d'urls à visiter. Néanmoins, les deux derniers paramètres ne sont pas obligatoires
- Lis le contenu du fichier seed et enregistre ses différents urls dans une liste `frontier`
- Prends le premier élément de ma liste de frontier, vérifie s'il a déja été parser et donc est dans la liste `crawled_webpages`. Si non alors, le parse en s'assurant que c'est bien une url et qu'on a les autorisations pour le crawler
- Lorsque celui-ci est parser, il est supprimer de `frontier` et ajouté dans une liste appélée `crawled_webpages`
- Les urls trouvées sont ajoutées à `frontier` si ceux ci n'y sont pas déjà.

## Démarrage rapide

cloner le dépôt git :

```
git clone https://gitlab.com/SAWAJul/indexation-web.git
```

Se placer dans le dossier de l'application, via les commandes:

```
cd indexation-web/TP1
```

Enfin lancer le crawler avec :

```
python3 main.py seed.txt --politeness_delay 3 --max_urls 50
```

## Auteur

SAWADOGO Julien